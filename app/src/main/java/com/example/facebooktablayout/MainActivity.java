package com.example.facebooktablayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.TableLayout;

import com.example.facebooktablayout.fragment.Notifications;
import com.example.facebooktablayout.fragment.PagerAdapter;
import com.example.facebooktablayout.fragment.Profile;
import com.example.facebooktablayout.fragment.Setting;
import com.example.facebooktablayout.fragment.TabGroupsFragment;
import com.example.facebooktablayout.fragment.TabHomeFragment;
import com.example.facebooktablayout.fragment.Watch;
import com.example.facebooktablayout.help.FragmentHelper;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
    private TabLayout appTab;
    private ViewPager TabPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initDefaultTabBody();
        /*selection();*/
        ScrollViewPager();

    }
    private void initView(){
        appTab=findViewById(R.id.homeTab);
        TabPager = findViewById(R.id.body_Tab);
    }
    private void initDefaultTabBody(){
        FragmentHelper.addFragment(this,R.id.body_Tab,new TabHomeFragment());
    }
    public void selection(){
        appTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                Fragment fragment= null;
                switch (pos)
                {
                    case 0:
                        fragment=new TabHomeFragment();
                        break;
                    case 1:
                        fragment=new TabGroupsFragment();
                        break;
                    case 2:
                        fragment = new Watch();
                        break;
                    case 3:
                        fragment = new Profile();
                        break;
                    case 4:
                        fragment = new Notifications();
                        break;
                    case 5:
                        fragment = new Setting();
                        break;
                }
                FragmentHelper.replace(MainActivity.this,R.id.body_Tab,fragment);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public void ScrollViewPager() {
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), appTab.getTabCount());
        appTab.setupWithViewPager(TabPager);
        TabPager.setAdapter(pagerAdapter);
    }
}
