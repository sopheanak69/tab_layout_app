package com.example.facebooktablayout.fragment;

import android.icu.text.Transliterator;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {
    private int count;
    public CharSequence[] tabTitle= {"Home","Group","Watch","Me","Msg","Info"};
    public PagerAdapter(FragmentManager fm,int count) {
        super(fm);
        this.count=count;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position)
        {
            case 0:
                fragment=new TabHomeFragment();
                break;
            case 1:
                fragment=new TabGroupsFragment();
                break;
            case 2:
                fragment = new Watch();
                break;
            case 3:
                fragment = new Profile();
                break;
            case 4:
                fragment = new Notifications();
                break;
            case 5:
                fragment = new Setting();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitle[position];
    }
}
