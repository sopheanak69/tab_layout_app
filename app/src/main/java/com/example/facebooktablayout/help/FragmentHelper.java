package com.example.facebooktablayout.help;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentHelper {
    static public void addFragment(AppCompatActivity appCompatActivity, @IdRes int container, Fragment fragment){
        FragmentManager fragmentManager =appCompatActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        fragmentTransaction.add(container,fragment);
        fragmentTransaction.commit();
    }
    static public void replace(AppCompatActivity appCompatActivity, @IdRes int container, Fragment fragment){
        FragmentManager fragmentManager =appCompatActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        fragmentTransaction.replace(container,fragment);
        fragmentTransaction.commit();
    }
}
